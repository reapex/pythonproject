import time

questions = [
    "Quel est le nom du 41ème Président des Etats-unis ?",
    "Quelle est la hauteur de la Tour Eiffel ?",
    "Quelle langue n'est pas une langue officielle du Luxembourg ?",
    "Quelle capitale comporte le plus d'habitants ?",
    "Comment écrivons-nous 500 en chiffre romain ?",
    "Lequels de ces nombre est un nombre premier ?",
    "Quelle est la distance Terre-Lune ?",
    "Quelle capitale est située le plus au sud ?",
    "Quel était le prix d'une baguette en 1980 ?",
    "En pourcentage, combien représente la totalité des terres émergées de la Terre ?"
]

answer = [
    "Bill Clinton,George H. W. Bush,Barack Obama,Ronald Reagan,b",
    "304m,314m,324m,334m,c",
    "Luxembourgeois,Anglais,Français,Allemand,b",
    "Londres,Moscou,Paris,Mexico,b",
    "L,X,D,M,b",
    "3639,4111,4372,5932,b",
    "354 400km,398 300km,384 400km,328 300km,c",
    "Rome,Madrid,Lisbonne,Washington DC,c",
    "0.25 francs,1.23 francs,1.67 francs,2.42 francs,c",
    "5%,29%,39%,62%,b"
]

# Initialisation du compteur.
compteur = 0

# Fonction lancée au démarrage.
def main():
    print(">>> Bienvenue dans le jeu Trivia ! <<<\n\nLe principe du jeu est simple, vous devez essayer d'avoir le plus de bonnes réponses possibles. Une question est posée et quatre réponses possibles vous y sont données.\n\n")
    for i in range(10):
        question(i)

# Fonction pour poser les questions.
def question(num):
    global compteur  # Importation du compteur.
    print(num+1, "-", questions[num])  # Afficher la question.
    correctAnswer = answer[num].split(',')  # Tableau des réponses possibles.

    # On donne les réponses.
    userInput = input("\na) " + correctAnswer[0] + "\nb) " + correctAnswer[1] +
                      "\nc) " + correctAnswer[2] + "\nd) " + correctAnswer[3] + "\n\nSaisissez votre réponse (a, b, c, d) : ")

    # On passe la chaine de caractere en miniscule.
    userInput = userInput.lower()

       # On vérifie si la réponse est correct
    if userInput == correctAnswer[4]:
        compteur = compteur + 1  # Incrémentation du compteur
        print("\n>>> Bonne réponse :", compteur, "/", num+1, "\n\n")
        time.sleep(3)
    else:
        print("\n>>> Mauvaise réponse :", compteur, "/", num+1, "\n\n")
        time.sleep(3)

if __name__ == '__main__':
    main()